import { Component, OnDestroy } from '@angular/core';
import { interval, Subject, empty, of } from 'rxjs';
import { startWith, switchMap, takeUntil } from 'rxjs/operators';
import { Insomnia } from '@ionic-native/insomnia/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage implements OnDestroy {
  destroySub$ = new Subject();
  percent = 0;
  specifiedRestTime = '00:01:30';

  constructor(private insomnia: Insomnia) {}

  startTimer() {
    this.destroySub$.next();
    const [_, minutes, seconds] = this.specifiedRestTime.split(':');
    const totalSeconds = Number(seconds) + Number(minutes) * 60;

    this.insomnia.keepAwake();

    interval(1000)
      .pipe(
        startWith(-1),
        switchMap(progress => {
          progress = progress + 1;
          const percent = Math.floor((progress / totalSeconds) * 100);

          return percent === 100 ? empty() : of(percent);
        }),
        takeUntil(this.destroySub$)
      )
      .subscribe(percent => (this.percent = percent), null, () =>
        this.insomnia.allowSleepAgain()
      );
  }

  ngOnDestroy() {
    this.destroySub$.next();
    this.destroySub$.complete();
  }
}
